import React ,{ Component } from 'react'
import { 
    BrowserRouter as Router, 
    Switch,
    Route, 
    Link 
  } from "react-router-dom";

import Home from '../pages/home.pages';
import About from '../pages/about.pages';
import Gallery from '../pages/gallery.pages';


import './navBar.components.organisms.scss';

class NavBar extends Component {
    

    render() {
        return(
        <Router>
          <nav>
            <ul>
              <li className='logo right'>My Gallery</li>
              <li>
                <Link 
                  to={'/blog/about'}
                  activeClassName="nav-link-active" 
                  className='nav-list left'>
                    About
                </Link>
              </li>
              <li>
                <Link
                  to={'/blog/gallery'}
                  activeClassName="nav-link-active" 
                  className='nav-list left'>
                    Photos
                </Link>
              </li>
              <li>
                <Link
                 to={'/blog'} 
                 activeClassName="nav-link-active"
                 className='nav-list left'>
                   Home
                </Link>
              </li>
            </ul>
          </nav>
         
          <Switch>
            <Route exact path='/blog' component={Home} />
            <Route path='/blog/gallery' component={Gallery} />
            <Route path='/blog/about' component={About} />
          </Switch>
        
        </Router>
                    
             
            
        )
    }
}

export default NavBar;




